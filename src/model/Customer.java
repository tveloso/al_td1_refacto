package model;

import java.util.ArrayList;

public class Customer {
    private String name;
    private ArrayList<Rental> rentals;
    private double totalAmount;
    private int rentalPoints;

    public Customer(String name){
        this.name = name;
        this.rentals = new ArrayList<Rental>();
        this.totalAmount = 0;
        this.rentalPoints = 0;
    }

    public void addRental(Movie movie, int daysRented){
        Rental rental = new Rental(movie,daysRented);
        this.totalAmount += rental.getAmount();
        this.rentalPoints += rental.getPoints();
        this.rentals.add(rental);
    }

    public String statement(){
        String statement;
        statement = "Rental Record for "+this.name+"\n";
        for(Rental rental : this.rentals){
            statement += "\t" + rental.getMovie().getTitle()+"\t" + String.valueOf(rental.getAmount()) + " \n";
        }
        statement += "Amount owned is " + String.valueOf(this.totalAmount) + "\n";
        statement += "You earned " + String.valueOf(this.rentalPoints) + " frequent renter points";
        return statement;
    }
}
