package model;

public class Main {
    public static void main(String[] args) {
        Movie rogueOne = new Movie("Rogue One",new PriceCodeNewRelease());
        Movie reineNeige = new Movie("Reine des neiges",new PriceCodeChildren());
        Movie starWars3 = new Movie("Star Wars III",new PriceCodeRegular());

        Customer customer = new Customer("client1");
        customer.addRental(rogueOne,5);
        customer.addRental(reineNeige,7);
        customer.addRental(starWars3,4);

        System.out.println(customer.statement());
    }
}
