package model;

public interface PriceCode {
    double computeAmount(int daysRented);
    int computePoints(int daysRented);
}
