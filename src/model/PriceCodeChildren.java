package model;

public class PriceCodeChildren implements PriceCode{
    @Override
    public double computeAmount(int daysRented) {
        double amount = 1.5;
        if(daysRented > 3){
            amount += (daysRented - 3) * 1.5;
        }
        return amount;
    }

    @Override
    public int computePoints(int daysRented) {
        return 1;
    }
}
