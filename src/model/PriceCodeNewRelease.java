package model;

public class PriceCodeNewRelease implements PriceCode{
    @Override
    public double computeAmount(int daysRented) {
        return daysRented * 3;
    }

    @Override
    public int computePoints(int daysRented) {
        int points = 1;
        if(daysRented > 1){
            points += 1;
        }
        return points;
    }
}
