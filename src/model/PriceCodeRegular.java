package model;

public class PriceCodeRegular implements PriceCode{
    @Override
    public double computeAmount(int daysRented) {
        double amount = 2;
        if(daysRented > 2){
            amount += (daysRented - 2) * 1.5;
        }
        return amount;
    }

    @Override
    public int computePoints(int daysRented) {
        return 1;
    }
}
