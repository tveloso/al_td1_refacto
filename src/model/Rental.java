package model;

public class Rental {
    private Movie movie;
    private int daysRented;
    private double amount;
    private int points;

    public Rental(Movie movie, int daysRented){
        this.movie = movie;
        this.daysRented = daysRented;
        this.amount = this.movie.getPriceCode().computeAmount(daysRented);
        this.points = this.movie.getPriceCode().computePoints(daysRented);
    }

    public Movie getMovie(){
        return this.movie;
    }

    public double getAmount(){
        return this.amount;
    }

    public int getPoints(){
        return this.points;
    }
}
