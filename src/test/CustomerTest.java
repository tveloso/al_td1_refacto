package test;

import static org.junit.Assert.assertEquals;

import model.*;
import org.junit.Test;

public class CustomerTest {
    @Test
    public void evaluateStatement() {
        Movie rogueOne = new Movie("Rogue One",new PriceCodeNewRelease());
        Movie reineNeige = new Movie("Reine des neiges",new PriceCodeChildren());
        Movie starWars3 = new Movie("Star Wars III",new PriceCodeRegular());

        Customer customer = new Customer("client1");
        customer.addRental(rogueOne,5);
        customer.addRental(reineNeige,7);
        customer.addRental(starWars3,4);

        assertEquals("Rental Record for client1\n\tRogue One\t15.0 \n\tReine des neiges\t7.5 \n\tStar Wars III\t5.0 \nAmount owned is 27.5\nYou earned 4 frequent renter points", customer.statement());
    }
}
